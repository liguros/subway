#!/bin/bash
set -eo pipefail

# shellcheck source=stage-functions.sh
. /tmp/stage-functions.sh

echo "[ST1PYU] - Updating python to the latest version ..."
# update python
pyver=3.x
pyver_single=${pyver:0:1}
# pyver is now set to major version of specified python in metro config. The version specified in
# metro config is the "default" version enabled for the system.

# disable python2 as we no longer allow it
# when we have python3 as a default, we'll want to enable something like this (conditional):
#if [ "$pyver_single" == "2" ]; then
#echo "[ST1PY2U] - Upgrading python-2 first, since it is present on the system ..."
#echo "[ST1PY2U] - Collecting packages that need to be upgraded ..."
#emerge -pvtukb =dev-lang/python-2*
#echo "[ST1PY2U] - Performing update ..."
#emerge -qjukb =dev-lang/python-2* || die "[ST1PY2U] - Error: failed to upgrade python-2"
#echo "[ST1PY2U] - The update of python-2 finished successfully."
#fi
echo "[ST1PY3U] - Upgrading python-3 ..."
echo "[ST1PY3U] - Collecting packages that need to be upgraded ..."
emerge -pvtukb =dev-lang/python-3*
echo "[ST1PY3U] - Performing update ..."
emerge -qjvukb =dev-lang/python-3* || die "[ST1PY3U] - Error: failed to upgrade python-3"
echo "[ST1PY3U] - The update of python-3 finished successfully."
latest_python3=$(eselect python list --python3 | grep python |awk '{print $2}' | sort | tail -n 1)
oldest_python3=$(eselect python list --python3 | grep python |awk '{print $2}' | sort | head -n 1)
oldest_python3_pkg=python-${oldest_python3:6:4}
oldest_python3_target=${oldest_python3/./_}
latest_python3_target=${latest_python3/./_}
if [ "$latest_python3" != "$oldest_python3" ]; then
        # switch to correct python
        echo "Switching to latest python: ${latest_python3}"
        eselect python set "${latest_python3}" || die "[ST1PY3U] - Error: selecting correct python after python upgrade"
        # add latest python to PYTHON_TARGETS
        #echo "PYTHON_TARGETS=${latest_python3_target}" >> /etc/portage/make.conf
        #echo "PYTHON_SINGLE_TARGET=${latest_python3_target}" >> /etc/portage/make.conf
        sed -i '/^PYTHON_TARGETS\|^PYTHON_SINGLE_TARGET/d' /etc/portage/make.conf
        echo 'PYTHON_TARGETS="python3_11 python3_12 python3_13"' >> /etc/portage/make.conf
        echo 'PYTHON_SINGLE_TARGET="python3_12"' >> /etc/portage/make.conf

        MAKEOPTS=-j1 emerge -quDNkb -j"${NPROC}" @world --exclude glibc --exclude gcc || die "[ST1PY3U] - Error: rebuilding @world with new python"
        emerge -C =dev-lang/"${oldest_python3_pkg}"* || die "[ST1PY3U] - Error: unmerging oldest_python3 failed"
        eselect python cleanup
else
        # add oldest/current python to PYTHON_TARGETS
        sed -i '/^PYTHON_TARGETS\|^PYTHON_SINGLE_TARGET/d' /etc/portage/make.conf
        #echo "PYTHON_TARGETS=${oldest_python3_target}" >> /etc/portage/make.conf
        #echo "PYTHON_SINGLE_TARGET=${oldest_python3_target}" >> /etc/portage/make.conf
        echo 'PYTHON_TARGETS="python3_11"' >> /etc/portage/make.conf
        echo 'PYTHON_SINGLE_TARGET="python3_11"' >> /etc/portage/make.conf
fi

# Debug python list
eselect python list

echo "[ST1PY3U] - Updating python - DONE"
