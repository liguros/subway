#!/bin/bash -xe

# shellcheck source=stage-functions.sh
. /tmp/stage-functions.sh

# emerging local-gen before usage
eopts_quiet="-q"
#eopts_single="-1"
eopts_binary=(-bk --buildpkg-exclude "virtual/*")
eopts="${eopts_quiet} ${eopts_binary[@]} --verbose-conflicts"

sed -i "s/release:.*/release: ${LIG_RELEASE}/" /etc/ego/ego.yml
ego sync
# liguros/bugs#231
touch /etc/portage/make.profile/parent

case $LIG_ARCH in
    x86_64)
        if ego profile list|grep "x86-64bit"; then
                ego profile arch x86-64bit
        else
                ego profile arch amd64
        fi
        ego profile subarch generic_64
        ;;
    armv8l)
        ego profile arch arm
        ego profile subarch armv7a_hardfp
        ;;
    armv7l)
        ego profile arch arm
        ego profile subarch armv7a_hardfp
        ;;
    aarch64)
        ego profile arch arm64
        ego profile subarch arm64_generic
        ;;
    *)
esac
ego profile build current
ego profile flavor core

USE=""

emerge $eopts --oneshot sys-apps/locale-gen
sed -i /etc/locale.gen -e "s/^#en_US/en_US/g"
locale-gen

# Temporary fix for stable libressl build error. Fixed in develop
if [ "$LIG_VARIANT" = "libressl" ]; then
	echo "net-misc/curl -http2 -libressl -nss curl_ssl_gnutls" >> /etc/portage/package.use
else
	echo "net-misc/curl -http2 -nss" >> /etc/portage/package.use
fi

USE="-curl_quic_openssl -http3 -quic" emerge $eopts --oneshot net-misc/curl || die "Could not emerge curl"

# Remove all mix-ins and add only libressl/openssl + multilib if available
sed -i "/^liguros-repo:.*\/mix-ins\/.*/d" /etc/portage/make.profile/parent
ego profile mix-ins "$LIG_VARIANT"
# add no-systemd mix-in to avoid udev x eudev conflicts
ego profile mix-ins no-systemd
case $LIG_ARCH in
    x86_64)
        # Always build multilib stage for x84_64
        if ego profile list|grep " multilib"; then
            ego profile mix-ins multilib
        fi
        ;;
    *)
        ego profile mix-ins no-multilib
        ;;
esac


#######
# BUILDING PYTHON3 ONLY
# This is not working
#ego profile mix-ins +python3-only
# need to add pythons to make.conf
#echo 'USE="-python_targets_python2_7 -python_single_target_python2_7"' >> /etc/portage/make.conf
#######
if [ -f /etc/portage/make.conf ]; then 
    rm /etc/portage/make.conf
fi

# use python3 if available - if not available, assume we are OK with python3 only
#a=$(eselect python list | sed -n -e '1d' -e 's/^.* \(python[23]\..\).*$/\1/g' -e '/python3/p') #'
a=$(eselect python list | sed -n -e '1d' -e 's/.*\(python[23]\..*\)/\1/g' -e '/python3/p' | head -n 1)
echo "$a"
if [ "$a" != "" ]
then
    eselect python set "$a"
    eselect python cleanup
fi

echo "[ST3EGO] - Reinstall ego"
emerge $eopts -1 ego || die "[ST3EGO] - Error: failed to emerge"
echo "[ST3PORTAGE] - Reinstall portage"
emerge $eopts -1 portage || die "[ST3PORTAGE] - Error: failed to emerge"
export USE="${USE} bindist -http2"
# handle perl upgrades
emerge -pvu --nodeps dev-lang/perl|grep dev-lang/perl; ec=$? # This is 0 if there is perl upgrade available, 1 if not
if [ $ec = 0 ]; then
    emerge $eopts -1u --nodeps dev-lang/perl
    echo "Perl was upgraded let's upgrade deps"
    perl-cleaner --all -- ${eopts} || die "Error: failed to run perl-cleaner"
fi

latest_perl=$(portageq best_version /  dev-lang/perl|cut -f3 -d"-")
latest_perlmod=$(ls -1r /usr/lib/perl5/vendor_perl |head -1)

# Remove ".0" if it exists from the Perl version
if [ "${latest_perl%%.0}" != "$latest_perlmod" ]; then
    echo "Perl modules are not the same version as latest perl installed"
    perl-cleaner --all -- ${eopts} || die "[ST3PERL] - Error: failed to run perl-cleaner"
fi

echo "[ST3SYSTEM] - Upgrading system"
emerge $eopts -e system --usepkg-exclude sys-libs/pam -pvt
echo "[ST3SYSTEM] - Upgrading glibc and gcc first" # liguros/bugs#321
emerge $eopts -1 glibc gcc --usepkg-exclude sys-libs/pam || die "[ST3SYSTEM] Error: failed to emerge gcc and glibc"
echo "[ST3SYSTEM] - Upgrading @system without glibc and gcc"
emerge $eopts -e system --usepkg-exclude sys-libs/pam --exclude glibc --exclude gcc || die "[ST3SYSTEM] Error: failed to emerge system"

# zap the world file and emerge packages
rm -f /var/lib/portage/world || exit 2

# Install packages that have service file of the same name
# these get added in the $services_add variable automatically
packages_services="metalog chrony"

# Install packages for the stage3
packages=""

# Remove old kernel sources first
# packages_kernel is defined in stage-functions.sh
echo "[ST3KERNEL] - Upgrading kernel"
emerge -Cq ${packages_kernel}
emerge $eopts ${packages_kernel} --usepkg-exclude sys-kernel/${packages_kernel} || die "[ST3KERNEL] Error: failed to emerge"
KERNEL=$(portageq best_visible / ${packages_kernel})
KERNEL_VER=${KERNEL/*-/}
echo "[ST3KERNEL] - Upgrading kernel v${KERNEL_VER} - DONE"

echo "[ST3MISC] - Installing misc packages"
if [ -n "$packages_services" ]; then
	emerge $eopts ${packages_services} || die "[ST3MISCSERVICES] - Error: failed to emerge"
fi
if [ -n "$packages" ]; then
    emerge $eopts ${packages} || die "[ST3MISCPACKAGES] - Error: failed to emerge"
fi

# add default runlevel services
services_add="sshd net.eth0 ${packages_services}"

cd /etc/init.d
ln -s netif.tmpl net.eth0

for service in $services_add
do
    s=${service%%:*}
    l=${service##*:}
    [ "$l" == "$s" ] && l="default"
        if [ -e /etc/init.d/"$service" ]; then
            if [ "$service" == "net.eth0" ]; then
            echo template=udhcpc > /etc/conf.d/net.eth0
            fi
            rc-update add "$s" ${l}
        fi
done

# remove default runlevel services
services_del="dhcpcd"

for service in $services_del
do
    s=${service%%:*}
    l=${service##*:}
    [ "$l" == "$s" ] && l="default"
        if [ -e /etc/init.d/"$service" ]; then
            rc-update del "$s" ${l}
        fi
done
echo "[ST3MISC] - Installing misc packages - DONE"

echo "[ST3MISC] - Setting some defaults"
if [ -e /usr/share/eselect/modules/vi.eselect ] && [ -e /bin/busybox ]; then
    eselect vi set busybox
fi
echo "[ST3MISC] - Setting some defaults - DONE"

echo "[ST3CLEANUP] - Performing some cleanup"
# Perform some cleanup
rm -rf /tmp/*
rm /etc/._cfg*
rm /var/log/emerge*
rm -rf /var/log/portage
rm /var/log/wtmp
find / -user 125 -group 125 -exec chown -h root:root {} \;
echo "[ST3CLEANUP] - Performing some cleanup - DONE"
