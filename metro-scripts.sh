[collect ../common.conf]
  ############################
  ## This is common.conf    ##
  ############################
  [collect ../fslayouts/funtoo/layout.conf]
    ############################
    ### This is fslayouts...  ###
    #############################

    [section path/mirror]

    # This is the path where we look for portage tree snapshots, which are tarballs
    # that contain the contents of an entire portage tree. In Metro, portage tree
    # snapshots may be either a simple tarball of file or a tarball of an active
    # git repository. By default, each build has its own snapshot directory, as
    # builds can use different portage trees.

    snapshot/subpath: $[target/build]/snapshots

    # This file defines the mirror filesystem layout used by funtoo.org, and the
    # default mirror filesystem layout for Metro.
    #
    # In Metro lingo, a "mirror" is a location on disk that holds all the stuff
    # that Metro needs as well as all the stuff it creates. So this means that this
    # file defines the relative paths and filenames of everything (relative to
    # path/mirror, defined in /etc/metro.conf) that Metro will create, and relative
    # paths and filenames of everything that Metro will require for its builds.

    # If we are building a binary-image or a virtual-image (stage tarball or
    # virtual machine template,) then these subpaths specify the directory
    # (relative to $[path/mirror]) where our files are kept.  Virtual images are
    # typically stored in a subdirectory of this path, while stage tarballs are
    # typically stored immediately within this directory.

    source/subpath: $[source/build]/$[source/arch_desc:zap]/$[source/subarch:zap]/$[source/version]
    target/subpath: $[target/build]/$[target/arch_desc:zap]/$[target/subarch:zap]/$[target/version]
    target/path: $[]/$[:target/subpath]
    source/path: $[]/$[:source/subpath]

    source/control: $[]/$[source/build]/$[source/arch_desc:zap]/$[source/subarch:zap]/.control
    target/control: $[]/$[target/build]/$[target/arch_desc:zap]/$[target/subarch:zap]/.control

    link/suffix: latest

    target: $[path/mirror]/$[:target/subpath]/$[:target/basename]
    target/link: $[]/$[target/build]/$[target/arch_desc]/$[target/subarch]/$[:target/latest]
    # points from "shortcut" symlink to the real file (tucked away in subdir)
    target/link/dest: $[target/version]/$[:target/basename]

    target/auxlink: $[]/$[target/build]/$[target]/$[target/arch_desc]/$[:target/full_latest]
    # points from "shortcut" auxlink to the real file
    target/auxlinkdest: ../../../$[:target/subpath:zap]/$[:target/basename]
    target/auxlink/enable: openvz

    [section strategy]

    seed: << $[path/mirror/target/control:zap]/strategy/seed
    build: << $[path/mirror/target/control:zap]/strategy/build
    ##############

  [section release]

  author: Daniel Robbins <drobbins@funtoo.org>

  [section target]

  compression: xz

  [section portage]

  FEATURES:
  SYNC: $[snapshot/source/remote]
  USE:

  [section profile]

  format: new
  path: core-kit:funtoo/1.0/linux-gnu
  arch: $[:path]/arch/$[target/arch_desc]
  subarch: $[:path]/arch/$[target/arch_desc]/subarch/$[target/subarch]

  [section version]

  python: 2.7

  [section snapshot]

  type: live
  compression: xz
  branch/tar: origin/master

  [section snapshot/source]

  type: meta-repo
  options:
  # stage2 does not have https support, thus this:
  sync_base_url: git://github.com/funtoo/{repo}

  [section metro]

  options:
  options/stage: cache/package
  target: gentoo

  [section baselayout]

  services: sshd

  [section multi]

  snapshot: snapshot

  [section files]

  motd/trailer: [

   >>> Send suggestions, improvements, bug reports relating to...

   >>> Funtoo Linux:        Funtoo Linux (http://www.funtoo.org)
  ]

  [collect ../multi-targets/$[multi/mode:zap]]
  ############################

[collect ../release-build.conf]
  ############################
  ## This is release-build.conf    ##
  ############################
  [section snapshot/source]

  name: meta-repo

  [section release]

  type: official
  ##############################

[collect ../packages/$[target/arch_desc].conf]
  ############################
  ## This is packages/x86-64bit.conf    ##
  ############################
  [section emerge]

  packages: [
          sys-kernel/debian-sources-lts
  ]
  ############################

[section profile]

build: $[:path]/build/current
flavor: $[:path]/flavor/core
mix-ins:
release: 19.1
