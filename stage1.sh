#!/bin/bash -xe
set -x 

# shellcheck source=stage-functions.sh
. /tmp/stage-functions.sh

# Switch from meta-repo to single repo
if [[ -f "/etc/portage/repos.conf/ego-core-kit" ]]
then
    curl https://gitlab.com/liguros/kit-fixups/-/raw/develop/profiles/migrate-repo.sh|bash
fi

eopts_quiet="-q"
eopts_single="-1"
eopts_binary=(-bk --buildpkg-exclude "virtual/*")
eopts="${eopts_quiet} ${eopts_single} ${eopts_binary[@]} --verbose-conflicts"

sed -i "s/release:.*/release: ${LIG_RELEASE}/" /etc/ego/ego.yml || die "Not a single-repo release, unable to continue."
ego sync

# If the profile is invalid make some corrections
# TODO - this is not working since ego profile lists also empty profile without returning error/notice
#if ego profile list|grep "None available"; then # this is true if profile is not setup correctly
    echo "Re-setting profile"
    case $LIG_ARCH in
        x86_64)
            ego profile arch amd64
            ego profile subarch generic_64
            # Always build multilib stage for x84_64
            if ego profile list|grep " multilib"; then
                ego profile mix-ins multilib
            fi
            ;;
        armv8l)
            ego profile arch arm
            ego profile subarch armv7a_hardfp
            ;;
        armv7l)
            ego profile arch arm
            ego profile subarch armv7a_hardfp
            ;;
        aarch64)
            ego profile arch arm64
            ego profile subarch arm64_generic
            ego profile mix-ins no-multilib
            ;;
        *)
    esac
#fi

FEATURES="$FEATURES nodoc noman noinfo"
export FEATURES

sed -i '/^PYTHON_TARGETS\|^PYTHON_SINGLE_TARGET/d' /etc/portage/make.conf
echo 'PYTHON_TARGETS="python3_11 python3_12 python3_13"' >> /etc/portage/make.conf
echo 'PYTHON_SINGLE_TARGET="python3_13"' >> /etc/portage/make.conf

echo "[ST1PORTAGE] - Upgrading portage"
emerge -1qu sys-apps/portage || die "[ST1PORTAGE] - Error: failed to emerge portage"
echo "[ST1EGO] - Upgrading ego"
emerge -1qu app-admin/ego || die "[ST1EGO] - Error: failed to emerge ego"

bash /tmp/stage1-pythonupdate.sh

emerge -q1 -j"$NPROC" --nodeps portage
ego sync --config-only

sed -i "/^liguros-repo:.*\/mix-ins\/.*/d" /etc/portage/make.profile/parent
ego profile flavor minimal
ego profile mix-ins "$LIG_VARIANT"
    case $LIG_ARCH in
        x86_64)
            # Always build multilib stage for x84_64
            if ego profile list|grep " multilib"; then
                ego profile mix-ins multilib
            fi
            ;;
        *)
            ego profile mix-ins no-multilib
            ;;
    esac

buildpkgs="$(python /tmp/stage1-pythonjunk.py) sys-apps/busybox"
export buildpkgs

ego profile list
echo "[ST1INFO] - Dump portage info"
emerge --info

emerge -pvu --nodeps dev-lang/perl|grep dev-lang/perl; ec=$? # This is 0 if there is perl upgrade available, 1 if not
if [ $ec = 0 ]; then
    export PERL_UPGRADE_AVAIL=1
    echo "[ST1PERL] There is an available upgrade to perl, upgrading now ..."
    echo "[ST1PERL] This is what will be upgraded ..."
    emerge $eopts -1u --nodeps -pv dev-lang/perl
    emerge $eopts -1u --nodeps dev-lang/perl || die "[ST1PERL] - Error: perl upgrade failed"
    echo "[ST1PERL] - Perl was upgraded let's upgrade deps"
    echo "[ST1PERL] - Emerging libxcrypt do satisfy latest glibc needs."
    emerge -pvt1q sys-libs/libxcrypt
    emerge -1q sys-libs/libxcrypt || die "[ST1PERL] - Error: libxcrypt failed to install/upgrade"
    perl-cleaner --all -- ${eopts} -pvt
    perl-cleaner --all -- ${eopts} || die "[ST1PERL] - Error: perl-cleaner failed to rebuild all packages"
else
    export PERL_UPGRADE_AVAIL=0
fi
echo "[ST1WORLD] - Upgrading world"
emerge $eopts -pvtuDN @world
emerge $eopts -uDN @world
echo "[ST1LIBS] - Rebuilding libraries"
emerge $eopts -pvt @preserved-rebuild
emerge $eopts @preserved-rebuild

echo "[ST1NEWROOT] - Preparing and building new stage1 rootfs"
export ROOT="/var/tmp/stage1root"
install -d $ROOT/var/git
ln -s /var/git/liguros-repo $ROOT/var/git/liguros-repo
install -d ${ROOT}
emerge --info
echo "[ST1NEWROOT] - Installing baselayout ..."
emerge --nodeps -q baselayout || die "[ST1NEWROOT] - Error: failed to emerge baselayout"
# shellcheck disable=SC2086
#emerge -pvu --nodeps dev-lang/perl|grep dev-lang/perl; ec=$? # This is 0 if there is perl upgrade available, 1 if not
# Sometimes texinfo in stage2 failes due to perl upgrade this solves it
if [ $PERL_UPGRADE_AVAIL == 1 ]; then
    echo "[ST1NEWROOTPERL] There is an available upgrade to perl, upgrading now ..."
    echo "[ST1NEWROOTPERL] This is what will be upgraded ..."
    emerge $eopts -1u --nodeps -pv dev-lang/perl
    emerge $eopts -1u --nodeps dev-lang/perl || die "[ST1PERL] - Error: perl upgrade failed"
    echo "[ST1PERL] - Perl was upgraded let's upgrade deps"
    echo "[ST1PERL] - Emerging libxcrypt do satisfy latest glibc needs."
    emerge -pvt1q sys-libs/libxcrypt
    emerge -1q sys-libs/libxcrypt || die "[ST1NEWROOTPERL] - Error: libxcrypt failed to install/upgrade"
    perl-cleaner --all -- ${eopts} -pvt
    perl-cleaner --all -- ${eopts} || die "[ST1NEWROOTPERL] - Error: perl-cleaner failed to rebuild all packages"
fi
echo "[ST1NEWROOTSYSTEM] - Installing system packages"
emerge --noreplace --oneshot -pvt $eopts ${buildpkgs} --verbose-conflicts
emerge --noreplace --oneshot $eopts ${buildpkgs} || die "[ST1NEWROOTSYSTEM] - Error: failed to emerge"
echo "[ST1NEWROOTSYSTEM] - System packages installed successfully."

install -d ${ROOT}/{proc,sys,dev}
rm $ROOT/var/git/liguros-repo
ego profile flavor core
# this can be remove once liguros/bugs#231 is resolved
install -d ${ROOT}/etc/portage/make.profile

# we need to pass liguros.conf so ego sync can work
install -d ${ROOT}/etc/portage/repos.conf
cp /etc/portage/repos.conf/liguros.conf ${ROOT}/etc/portage/repos.conf/liguros.conf

# remove any variables manually set or remnants in make.conf
rm /etc/portage/make.conf
# e.g. pythonupdate script setting PYTHON_TARGETS

# add default runlevel services
services_add="net.eth0"

cd /etc/init.d
ln -sf netif.tmpl net.eth0

for service in $services_add
do
        s=${service%%:*}
        l=${service##*:}
        [ "$l" == "$s" ] && l="default"
	if [ -e /etc/init.d/"$service" ]; then
        if [ "$service" == "net.eth0" ]; then
            echo template=udhcpc > /etc/conf.d/net.eth0
        fi
		rc-update add "$s" ${l}
	fi
done

# remove default runlevel services
services_del="netmount"

for service in $services_del
do
        s=${service%%:*}
        l=${service##*:}
        [ "$l" == "$s" ] && l="default"
	if [ -e /etc/init.d/"$service" ]; then
		rc-update del "$s" ${l}
	fi
done
