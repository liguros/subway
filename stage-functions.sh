#!/bin/bash
# Functions

# Print message and return exit code 1
# example usage:
# cmd || die "cmd failed"
die() {
    echo "$*" 1>&2 ; exit 1;
    }

BUILD_DATE=$(date +%F)
export BUILD_DATE
BUILD_DATE_EPOCH=$(date +%s)
export BUILD_DATE_EPOCH
LIG_ARCH=$(uname -m)
NPROC=$(nproc)
export NPROC
packages_kernel="debian-sources"
export packages_kernel

function checkOptions {
echo "Checking if release and variant variables are set correctly"
if [ -e "$LIG_RELEASE" ] || [ -e "$LIG_VARIANT" ]; then
	echo "Please set: "
	echo "LIG_RELEASE (stable/develop)"
	echo "LIG_VARIANT (openssl/libressl)."
	die "Error: No variant or release set"
fi

if [ "$LIG_RELEASE" == "stable" ] || [ "$LIG_RELEASE" == "develop" ]; then
	echo "Building $LIG_RELEASE release."
  if [ "$LIG_RELEASE" == "stable" ]; then
      LIG_BRANCH="master"
    else
      LIG_BRANCH="develop"
    fi
else
	echo "RELEASE can only be stable or develop."
	die "Error: Bad value for release"
fi

if [ "$LIG_VARIANT" == "openssl" ] || [ "$LIG_VARIANT" == "libressl" ]; then
	echo "Building $LIG_VARIANT variant."
else
	echo "LIG_VARIANT can only be openssl or libressl."
	die "Error: Bad value for variant"
fi
}

function checkBaseImage {
set -x
echo "Checking for baseimage for $LIG_RELEASE - $LIG_VARIANT [$LIG_ARCH]"
# Define the image name you want to check
image_name="liguros-stage3-$LIG_RELEASE-$LIG_VARIANT"

# Use incus image list to list all images in the local store
image_list=$(incus image list)

# Check if the image_name is in the image_list
if [[ $image_list =~ $image_name ]]; then
    echo "Image $image_name exists in the local incus image store."
else
    echo "Image $image_name does not exist in the local incus image store."
      echo "Baseimage is missing in local image storage"
  # Let's try if the latest CI job artifacts are downloadable if not, download latest image from build.liguros.net
  DOWNLOAD_URL="https://gitlab.com/liguros/subway/-/jobs/artifacts/${LIG_BRANCH}/raw/images/lxd-${LIG_RELEASE}-${LIG_VARIANT}.tar.xz?job=liguros-${LIG_RELEASE}-${LIG_VARIANT}-stage3"
  DOWNLOAD_URL_SUM="https://gitlab.com/liguros/subway/-/jobs/artifacts/${LIG_BRANCH}/raw/images/lxd-${LIG_RELEASE}-${LIG_VARIANT}.tar.xz.sum?job=liguros-${LIG_RELEASE}-${LIG_VARIANT}-stage3"
  ALT_DOWNLOAD_URL="https://build.liguros.net/${LIG_ARCH}/${LIG_RELEASE}/lxd-${LIG_VARIANT}-latest.tar.xz"
  FILENAME=lxd-${LIG_RELEASE}-${LIG_VARIANT}.tar.xz

  if curl -s -L --fail -I "${DOWNLOAD_URL}" ; then
    echo "Downloading from gitlab - ${DOWNLOAD_URL}"
    CURL_RETURN_CODE=0
    curl -sS -L --retry 5 "${DOWNLOAD_URL}" -o "${FILENAME}" || CURL_RETURN_CODE=$?
    if [ ${CURL_RETURN_CODE} -ne 0 ]; then
        echo "cURL connection failed with return code - ${CURL_RETURN_CODE}"
    else
        echo "cURL success - ${FILENAME}"
    fi

    CURL_RETURN_CODE=0
    curl -sS -L --retry 5 "${DOWNLOAD_URL_SUM}" -o "${FILENAME}".sum || CURL_RETURN_CODE=$?
    if [ ${CURL_RETURN_CODE} -ne 0 ]; then
        echo "cURL connection failed with return code - ${CURL_RETURN_CODE}"
    else
        echo "cURL success - ${FILENAME}.sum"
    fi
  else
    echo "Downloading from build.liguros.net - ${ALT_DOWNLOAD_URL}"
    CURL_RETURN_CODE=0
    curl -sS -L --retry 5 "${ALT_DOWNLOAD_URL}" -o "${FILENAME}" || CURL_RETURN_CODE=$?
    if [ ${CURL_RETURN_CODE} -ne 0 ]; then
        echo "cURL connection failed with return code - ${CURL_RETURN_CODE}"
    else
        echo "cURL success - ${FILENAME}"
    fi
    CURL_RETURN_CODE=0
    curl -sS -L --retry 5 "${ALT_DOWNLOAD_URL}.sum" -o "${FILENAME}".sum || CURL_RETURN_CODE=$?
    if [ ${CURL_RETURN_CODE} -ne 0 ]; then
        echo "cURL connection failed with return code - ${CURL_RETURN_CODE}"
    else
        echo "cURL success - ${FILENAME}.sum"
    fi
  fi

  echo "Checking the checksum of the image."
  sed -i -e "s/lxd-${LIG_VARIANT}-latest.tar.xz/${FILENAME}/" ${FILENAME}.sum
  if [ $(sha512sum --status -c ${FILENAME}.sum; echo $?) == 1 ]; then
    die "Error: Image checksum doesn't match."
  else
    echo "Image checksum ok - importing image to LXD"
    if [ "$FULL" == "true" ]; then
      incus image import -q "${FILENAME}" --alias liguros-stage3-"${LIG_RELEASE}-${LIG_VARIANT}"
    else
      incus image import -q "${FILENAME}" --alias liguros-stage2-"${LIG_RELEASE}-${LIG_VARIANT}"
    fi
    rm "${FILENAME}" "${FILENAME}".sum
  fi
fi
}

function buildStage1 {
set -x
echo "#########################"
echo "# Starting stage1 build #"
echo "#########################"
STAGE=$STAGE_STEP
BASEIMG=liguros-stage3-$LIG_RELEASE-$LIG_VARIANT
CONTAINER=liguros-pre-$STAGE-$LIG_RELEASE-$LIG_VARIANT
incus image list
echo "root:1000000:1000000000" > /etc/subgid
echo "root:1000000:1000000000" > /etc/subuid
cat /etc/subgid
cat /etc/subuid
/etc/init.d/incus status
/etc/init.d/incus restart
sleep 10 # wait for all services to startup to avoid error on the next command
/etc/init.d/incus status
incus profile show default
incus profile device add default root disk pool=default path=/
incus profile show default
incus init "$BASEIMG" "$CONTAINER" || die "Error: container failed to launch"
incus config set "$CONTAINER" image.description "Liguros - $LIG_RELEASE - $LIG_VARIANT [pre-stage-1]"
incus config set "$CONTAINER" environment.LIG_RELEASE "$LIG_RELEASE"
incus config set "$CONTAINER" environment.LIG_VARIANT "$LIG_VARIANT"
incus config set "$CONTAINER" environment.CONTAINERIZED "true"
incus config set "$CONTAINER" environment.TERM "xterm-color"
incus config set "$CONTAINER" environment.LANG "en_US.UTF-8"
incus config set "$CONTAINER" environment.LC_ALL "en_US.UTF-8"
incus config set "$CONTAINER" environment.LC_COLLATE "POSIX"
#printf "both 0 0\nboth 250 250" | incus config set "$CONTAINER" raw.idmap -
incus start "$CONTAINER"
sleep 10 # wait for all services to startup to avoid error on the next command
echo "Connecting directories for caching"
incus config device add "$CONTAINER" distfiles disk source=/var/cache/gitlab/distfiles path=var/cache/portage/distfiles
incus config device add "$CONTAINER" packages disk source=/var/cache/gitlab/packages-${STAGE}-${LIG_RELEASE}-${LIG_VARIANT} path=var/cache/portage/packages
if [ -e /var/git/meta-repo ]; then
  rm -rf /var/git/meta-repo
fi
incus config device add "$CONTAINER" liguros-repo disk source=/var/cache/gitlab/git-${LIG_RELEASE} path=var/git

#if ! incus image info stage1-publish-$LIG_RELEASE 2> /dev/null ; then
  echo "Creating container for stage1rootfs" >&2
  incus init --empty stage1-publish-$LIG_RELEASE-$LIG_VARIANT || die "Error: failed to init container"
  echo "Done."
#fi
sudo install -d --owner=1000000 --group=1000000 /var/lib/lxd/containers/stage1-publish-$LIG_RELEASE-$LIG_VARIANT/rootfs
incus config device add "$CONTAINER" stage1rootfs disk source=/var/lib/lxd/containers/stage1-publish-$LIG_RELEASE-$LIG_VARIANT/rootfs path=var/tmp/stage1root

echo "Uploading build scripts into container"
incus file push -q -r stage* "$CONTAINER"/tmp
sleep 1
echo "Starting the build script in container"
sudo incus exec "$CONTAINER" -- bash /tmp/${STAGE}.sh |tee $MIRROR_DIR/logs/stage1-${LIG_RELEASE}-${LIG_VARIANT}-${BUILD_DATE}.log || die "Error: container failed to execute command"
echo "###########################"
echo "# Stage1 build sucessfull #"
echo "###########################"
echo "Stopping container now."
incus stop "$CONTAINER" || die "Error: container failed to stop"
sleep 30 # wait for container to stop
echo "Publishing container as image to use in next step of stage2 generation"
incus publish -q stage1-publish-$LIG_RELEASE-$LIG_VARIANT --alias liguros-stage1-$LIG_RELEASE-$LIG_VARIANT --public --compression none || die "Error: failed to publish stage1 container"
echo "Removing temporary container stage1-publish-${LIG_RELEASE}"
incus rm stage1-publish-$LIG_RELEASE-$LIG_VARIANT || die "Error: failed to remove temp container stage1-publish-${LIG_RELEASE}-${LIG_VARIANT}"

echo "Exporting build image for next step"
incus image export -q liguros-stage1-$LIG_RELEASE-$LIG_VARIANT $MIRROR_DIR/liguros-stage1-$LIG_RELEASE-$LIG_VARIANT
echo "Removing the build image from LXD"
incus image alias rm -q liguros-stage1-$LIG_RELEASE-$LIG_VARIANT

echo "Removing build container"
incus rm $CONTAINER || die "Error: failed to remove container"

echo "Removing stage3 seed image alias"
incus image alias rm liguros-stage3-"${LIG_RELEASE}-${LIG_VARIANT}"

echo "Everything went fine, you should have a working stage1 image."
}

function buildStage2 {
echo "#########################"
echo "# Starting stage2 build #"
echo "#########################"
STAGE=$STAGE_STEP
BASEIMG=liguros-stage1-$LIG_RELEASE-$LIG_VARIANT
CONTAINER=liguros-${STAGE}-${LIG_RELEASE}-${LIG_VARIANT}
echo "Importing image to LXD"
incus image list
if [ incus image info $BASEIMG >/dev/null 2>/dev/null ]; then
  incus image alias rm $BASEIMG
fi
incus image list
incus image import -q $MIRROR_DIR/$BASEIMG.tar --alias $BASEIMG
incus image list
ls -alF $MIRROR_DIR
if [ -e "$MIRROR_DIR/$BASEIMG.tar" ]; then
    rm -v "$MIRROR_DIR/$BASEIMG.tar"
else
    echo "File does not exist: $MIRROR_DIR/$BASEIMG.tar"
fi

incus init $BASEIMG $CONTAINER || die "Error: container failed to launch"
incus config set $CONTAINER image.description "Liguros - $LIG_RELEASE - $LIG_VARIANT [stage2]"
incus config set $CONTAINER environment.LIG_RELEASE "$LIG_RELEASE"
incus config set $CONTAINER environment.LIG_VARIANT "$LIG_VARIANT"
incus config set $CONTAINER environment.CONTAINERIZED "true"
incus config set $CONTAINER environment.TERM "xterm-color"
incus config set $CONTAINER environment.LANG "en_US.UTF-8"
incus config set $CONTAINER environment.LC_ALL "en_US.UTF-8"
incus config set $CONTAINER environment.LC_COLLATE "POSIX"
#printf "both 0 0\nboth 250 250" | incus config set $CONTAINER raw.idmap -
incus start $CONTAINER
sleep 10 # wait for all services to startup to avoid error on the next command
incus exec $CONTAINER -- busybox udhcpc || die "Failed to start udhcpc"
echo "Connecting directories for caching"
incus config device add $CONTAINER distfiles disk source=/var/cache/gitlab/distfiles path=var/cache/portage/distfiles
incus config device add $CONTAINER packages disk source=/var/cache/gitlab/packages-${STAGE}-${LIG_RELEASE}-${LIG_VARIANT} path=var/cache/portage/packages
if [ -e /var/git/meta-repo ]; then
  rm -rf /var/git/meta-repo
fi
incus config device add "$CONTAINER" liguros-repo disk source=/var/cache/gitlab/git-${LIG_RELEASE} path=var/git

echo "Uploading build scripts into container"
incus file push -q -r stage* $CONTAINER/tmp
sleep 1
echo "Starting the build script in container"
sudo incus exec $CONTAINER -- bash /tmp/${STAGE}.sh |tee $MIRROR_DIR/logs/stage2-${LIG_RELEASE}-${LIG_VARIANT}-${BUILD_DATE}.log || die "Error: container failed to execute command"
echo "###########################"
echo "# Stage2 build sucessfull #"
echo "###########################"
echo "Stopping container now."
incus stop $CONTAINER || die "Error: container failed to stop"
sleep 30
echo "Publishing container as image for next step"
incus publish -q $CONTAINER --alias $CONTAINER --compression none || die "Error: failed to publish container"

echo "Exporting build image for next step"
incus image export $CONTAINER $MIRROR_DIR/$CONTAINER
echo "Removing the build image from LXD"
incus image alias rm $CONTAINER
incus image alias rm $BASEIMG

echo "Removing build container"
incus rm $CONTAINER || die "Error: failed to remove container"
}

function buildStage3 {
echo "#########################"
echo "# Starting stage3 build #"
echo "#########################"
STAGE=$STAGE_STEP
BASEIMG=liguros-stage2-$LIG_RELEASE-$LIG_VARIANT
CONTAINER=liguros-${STAGE}-${LIG_RELEASE}-${LIG_VARIANT}
echo "Importing image to LXD"
incus image import -q $MIRROR_DIR/$BASEIMG.tar --alias $BASEIMG
ls -alF $MIRROR_DIR
if [ -e "$MIRROR_DIR/$BASEIMG.tar" ]; then
    rm -v "$MIRROR_DIR/$BASEIMG.tar"
else
    echo "File does not exist: $MIRROR_DIR/$BASEIMG.tar"
fi
# Remove also artifact from the first job
if [ -e "$MIRROR_DIR/liguros-stage1-$LIG_RELEASE-$LIG_VARIANT.tar" ]; then
    rm -v "$MIRROR_DIR/liguros-stage1-$LIG_RELEASE-$LIG_VARIANT.tar"
else
    echo "File does not exist: $MIRROR_DIR/liguros-stage1-$LIG_RELEASE-$LIG_VARIANT.tar"
fi

incus init $BASEIMG $CONTAINER || die "Error: container failed to launch"
incus config set $CONTAINER image.description "Liguros - $LIG_RELEASE - $LIG_VARIANT [stage3]"
incus config set $CONTAINER environment.LIG_RELEASE "$LIG_RELEASE"
incus config set $CONTAINER environment.LIG_VARIANT "$LIG_VARIANT"
incus config set $CONTAINER environment.CONTAINERIZED "true"
incus config set $CONTAINER environment.TERM "xterm-color"
incus config set $CONTAINER environment.LANG "en_US.UTF-8"
incus config set $CONTAINER environment.LC_ALL "en_US.UTF-8"
incus config set $CONTAINER environment.LC_COLLATE "POSIX"
#printf "both 0 0\nboth 250 250" | incus config set $CONTAINER raw.idmap -
incus start $CONTAINER
sleep 10 # wait for all services to startup to avoid error on the next command
echo "Setting network address"
incus exec $CONTAINER -- busybox udhcpc
echo "Connecting directories for caching"
incus config device add $CONTAINER distfiles disk source=/var/cache/gitlab/distfiles path=var/cache/portage/distfiles
incus config device add $CONTAINER packages disk source=/var/cache/gitlab/packages-${LIG_RELEASE}-${LIG_VARIANT} path=var/cache/portage/packages
if [ -e /var/git/meta-repo ]; then
  rm -rf /var/git/meta-repo
fi
incus config device add "$CONTAINER" liguros-repo disk source=/var/cache/gitlab/git-${LIG_RELEASE} path=var/git

echo "Uploading build scripts into container"
incus file push -q -r stage* $CONTAINER/tmp
sleep 1
echo "Starting the build script in container"
sudo incus exec $CONTAINER -- bash /tmp/${STAGE}.sh |tee $MIRROR_DIR/logs/stage3-${LIG_RELEASE}-${LIG_VARIANT}-${BUILD_DATE}.log || die "Error: container failed to execute command"
echo "###########################"
echo "# Stage3 build sucessfull #"
echo "###########################"
echo "Stopping container now."
incus stop $CONTAINER || die "Error: container failed to stop"
sleep 30
echo "Publishing container as image for next step"
incus publish -q $CONTAINER --alias $CONTAINER --compression xz || die "Error: failed to publish container"
incus start $CONTAINER || die "Error: container failed to start"
echo "Removing linux kernel sources from the lxd image"
incus exec $CONTAINER -- emerge -C ${packages_kernel}
incus exec $CONTAINER -- emerge --depclean
echo "Removing modules and linux kernel from /boot inside the lxd image"
incus exec $CONTAINER -- find /boot/ -iname *debian* -exec rm {} \; || die "Error: removing kernel from /boot"
incus exec $CONTAINER -- rm -rf /lib/modules || die "Error: removing modules"
incus exec $CONTAINER -- rm -rf /var/log/portage || die "Error: removing portage logs"
incus exec $CONTAINER -- rm -rf /var/log/emerge.log || die "Error: removing portage logs"
incus exec $CONTAINER -- rm -rf /var/log/wtmp || die "Error: removing wtmp"
incus stop $CONTAINER || die "Error: container failed to stop"
sleep 30
echo "Publishing container as LXD image for next step"
incus publish -q $CONTAINER --alias $CONTAINER-lxd --compression xz || die "Error: failed to publish container"
incus image ls
echo "Removing build container"
incus rm $CONTAINER || die "Error: failed to remove container"
incus image alias rm $BASEIMG || die "Error: failed to remove base image - ${BASEIMG}"
echo "##############################################"
echo "# All went well! Full stage generation: done #"
echo "##############################################"
}

function createArchives {
echo "Exporting the stage3 image"
incus image export -q liguros-stage3-$LIG_RELEASE-$LIG_VARIANT $MIRROR_DIR/liguros-${LIG_VARIANT}-${BUILD_DATE} || die "Error: failed to export image"
incus image alias rm liguros-stage3-$LIG_RELEASE-$LIG_VARIANT

echo "Creating archives"
cd ${MIRROR_DIR} || die "Error: failed to change directory"
touch ${BUILD_DATE}
tar --acls --xattrs -xaf liguros-${LIG_VARIANT}-${BUILD_DATE}.tar.xz
rm liguros-${LIG_VARIANT}-${BUILD_DATE}.tar.xz
if [ -e rootfs/var/git/liguros-repo ]; then
  rm -rf rootfs/var/git/liguros-repo
fi
if [ -e rootfs/var/git/meta-repo ]; then
  rm -rf rootfs/var/git/meta-repo
fi
echo "- stage3 archive"
# correct uid/gid mappings with fuidshift
sudo fuidshift $(pwd)/rootfs b:1000000:0:65536
#TODO: add BUILD_DATE to the archive
sudo tar --acls --xattrs --one-file-system -caf stage3-${LIG_RELEASE}-${LIG_VARIANT}.tar.xz -C rootfs .
rm -rf metadata.yaml rootfs templates


# Create missing templates dir and hostname.tpl file
echo "- lxd archive"
incus image export -q liguros-stage3-${LIG_RELEASE}-${LIG_VARIANT}-lxd $MIRROR_DIR/liguros-${LIG_RELEASE}-${LIG_VARIANT}-${BUILD_DATE} || die "Error: failed to export image"
incus image rm liguros-stage3-${LIG_RELEASE}-${LIG_VARIANT}-lxd

tar --acls --xattrs -xaf liguros-${LIG_RELEASE}-${LIG_VARIANT}-${BUILD_DATE}.tar.xz
rm liguros-${LIG_RELEASE}-${LIG_VARIANT}-${BUILD_DATE}.tar.xz
if [ -e rootfs/var/git/liguros-repo ]; then
  rm -rf rootfs/var/git/liguros-repo
fi
if [ -e rootfs/var/git/meta-repo ]; then
  rm -rf rootfs/var/git/meta-repo
fi
if [ ! -d templates ]; then
    mkdir templates
else
    echo "templates dir exists - not creating"
fi
if [ ! -f templates/hostname.tpl ]; then
    echo 'hostname="{{ container.name }}"' > templates/hostname.tpl
else
    echo "hostname.tpl exists - not creating"
fi

# re-create metadata.yaml
METADATA="metadata.yaml"
echo "architecture: $LIG_ARCH" > $METADATA
echo "creation_date: $BUILD_DATE_EPOCH" >> $METADATA
echo "properties:" >> $METADATA
echo "  description: Liguros $LIG_RELEASE - $LIG_VARIANT [$LIG_ARCH] ($BUILD_DATE)" >> $METADATA
echo "  os: LiGurOS" >> $METADATA
echo "  release: $LIG_RELEASE - $LIG_VARIANT" >> $METADATA
echo "templates:" >> $METADATA
echo "  /etc/hostname:" >> $METADATA
echo "    when:" >> $METADATA
echo "      - start" >> $METADATA
echo "    template: hostname.tpl" >> $METADATA

# Disable terminals and serial consoles in LXD (liguros/bugs#97)
echo "Updating inittab..."
sed -i -e '/getty/s/^#*/#/' rootfs/etc/inittab

# correct uid/gid mappings with fuidshift
sudo fuidshift $(pwd)/rootfs b:1000000:0:65536

sudo tar --acls --xattrs --one-file-system -caf lxd-"${LIG_RELEASE}"-"${LIG_VARIANT}".tar.xz metadata.yaml rootfs templates
rm -rf metadata.yaml rootfs templates
}

function generateLinksSums {
cd ${MIRROR_DIR} || die "Error: failed to change directory"
echo "Updating sha512sums in ${MIRROR_DIR}"
sha512sum lxd-${LIG_RELEASE}-${LIG_VARIANT}.tar.xz > lxd-${LIG_RELEASE}-${LIG_VARIANT}.tar.xz.sum
sha512sum stage3-${LIG_RELEASE}-${LIG_VARIANT}.tar.xz > stage3-${LIG_RELEASE}-${LIG_VARIANT}.tar.xz.sum
echo "All done."
}
