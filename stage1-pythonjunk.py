#!/usr/bin/python

import os,portage

# this loads files from the profiles ...
# wrap it here to take care of the different
# ways portage handles stacked profiles
def scan_profile(file):
        return portage.stack_lists( [portage.grabfile_package(os.path.join(x, file)) for x in portage.settings.profiles], incremental=1);

# loaded the stacked packages / packages.build files
pkgs = scan_profile("packages")
buildpkgs = scan_profile("packages.build")

# go through the packages list and strip off all the
# crap to get just the <category>/<package> ... then
# search the buildpkg list for it ... if it's found,
# we replace the buildpkg item with the one in the
# system profile (it may have <,>,=,etc... operators
# and version numbers)

for idx in range(0, len(pkgs)):
        try:
                bidx = buildpkgs.index(portage.dep.Atom.getkey(pkgs[idx]))
                buildpkgs[bidx] = pkgs[idx]
                if buildpkgs[bidx][0:1] == "*":
                        buildpkgs[bidx] = buildpkgs[bidx][1:]
        except: pass

for b in buildpkgs: print(b)

