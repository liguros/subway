TL;DR

You need LXD installed, seed stage3, lxb binary and lxd profiles:
 - default:
 
 ```
 config:                                                                                                                                                                                                            
  environment.LANG: en_US.UTF-8                                                                                                                                                                                    
  environment.LC_ALL: en_US.UTF-8                                                                                                                                                                                  
  environment.LC_COLLATE: POSIX                                                                                                                                                                                    
  migration.incremental.memory: "true"                                                                                                                                                                             
  raw.lxc: lxc.mount.entry = none dev/shm tmpfs rw,nosuid,nodev,create=dir                                                                                                                                         
description: Default LXD profile                                                                                                                                                                                   
devices:                                                                                                                                                                                                           
  eth0:                                                                                                                                                                                                            
    name: eth0                                                                                                                                                                                                     
    nictype: bridged                                                                                                                                                                                               
    parent: lxdbr0                                                                                                                                                                                                 
    type: nic                                                                                                                                                                                                      
  root:                                                                                                                                                                                                            
    path: /                                                                                                                                                                                                        
    pool: default                                                                                                                                                                                                  
    type: disk                                                                                                                                                                                                     
name: default                                                                                                                                                                                                      
used_by: []                   
 ```
 
 - distfiles:
```
config: {}
description: ""
devices:
  distfiles:
    path: /var/cache/portage/distfiles
    source: /var/tmp/subway/distfiles
    type: disk
name: distfiles
used_by: []

```

- meta-repo:
```
config: {}
description: ""
devices:
  meta-repo:
    path: /var/git
    source: /var/tmp/subway/git
    type: disk
name: meta-repo
used_by: []
```

- packages-stage1
```
config: {}
description: ""
devices:
  packages:
    path: /var/cache/portage/packages
    source: /var/tmp/subway/packages-stage1
    type: disk
name: packages-stage1
used_by: []
```
and the same for packages-stage2 and packages-stage3 with the source adjusted accordingly.

Use as:
There is now `full-build.sh` script that calls all these scripts and steps noted below:
Stage1 (for the first generation a seed stage3 is needed)
```
./lxb -f lxfile-stage1.yml --verbose
```

Stage2 the stage1 is generated in /var/tmp/stage1root this needs to be copied to a temp. container and imported as an image.
sh ./pre-stage1-extract.sh

After this is done, the stage2 generation can be run using:

```
./lxb -f lxfile-stage2.yml --verbose
```

followed by stage3 generation

```
./lxb -f lxfile-stage3.yml --verbose
```

You should now have your generated stages:
```
lxc image ls subway --format=csv
liguros-subway-pre-stage1,db2bc163fda0,yes,Liguros - Subway pre-Stage-1,x86_64,662.85MB,"May 13, 2019 at 8:28am (UTC)"
liguros-subway-stage1,68d733c6fb15,yes,,x86_64,267.25MB,"May 13, 2019 at 8:30am (UTC)"
liguros-subway-stage2,1f423bc21ada,yes,Liguros - Subway Stage-2,x86_64,240.65MB,"May 13, 2019 at 8:34am (UTC)"
liguros-subway-stage3,9f0b3d641100,yes,Liguros - Subway Stage-3,x86_64,370.65MB,"May 13, 2019 at 8:50am (UTC)"
```
