#!/bin/bash -xe

# shellcheck source=stage-functions.sh
. /tmp/stage-functions.sh

eopts_quiet="-q"
#eopts_single="-1"
eopts_binary=(-bk --buildpkg-exclude "virtual/*")
eopts="${eopts_quiet} ${eopts_binary[@]} --verbose-conflicts"

sed -i "s/release:.*/release: ${LIG_RELEASE}/" /etc/ego/ego.yml
ego sync
# liguros/bugs#231
touch /etc/portage/make.profile/parent

case $LIG_ARCH in
    x86_64)
        if ego profile list|grep "x86-64bit"; then
            ego profile arch x86-64bit
        else
            ego profile arch amd64
        fi
        ego profile subarch generic_64
        ;;
    armv8l)
        ego profile arch arm
        ego profile subarch armv7a_hardfp
        ;;
    armv7l)
        ego profile arch arm
        ego profile subarch armv7a_hardfp
        ;;
    aarch64)
        ego profile arch arm64
        ego profile subarch arm64_generic
        ;;
    *)
esac

ego profile build current
ego profile flavor core

# emerging local-gen before usage
emerge $eopts --oneshot sys-apps/locale-gen
sed -i /etc/locale.gen -e "s/^#en_US/en_US/g"
locale-gen

# Temporary fix (PYTHON_PROBLEM)
# eselect is not present in stage2 - switching to portageq command
# also not doing any upgrade of python
# TODO  
current_python3=$(portageq best_visible / installed dev-lang/python | sed -E 's/.*\/([a-zA-Z]+)-([0-9]+\.[0-9]+)\..*/\1\2/; s/\./_/')
# add current python to PYTHON_TARGETS
#echo "PYTHON_TARGETS=${current_python3}" >> /etc/portage/make.conf
#echo "PYTHON_SINGLE_TARGET=${current_python3}" >> /etc/portage/make.conf
sed -i '/^PYTHON_TARGETS\|^PYTHON_SINGLE_TARGET/d' /etc/portage/make.conf
echo 'PYTHON_TARGETS="python3_11 python3_12 python3_13"' >> /etc/portage/make.conf
echo 'PYTHON_SINGLE_TARGET="python3_12"' >> /etc/portage/make.conf
# end of (PYTHON_PROBLEM)

export AUTOCLEAN="yes"
export CONFIG_PROTECT="-*"
export FEATURES="$FEATURES -collision-protect"

sed -i "/^liguros-repo:.*\/mix-ins\/.*/d" /etc/portage/make.profile/parent
ego profile flavor minimal
ego profile mix-ins "$LIG_VARIANT"
case $LIG_ARCH in
    x86_64)
        # Always build multilib stage for x84_64
        if ego profile list|grep " multilib"; then
            ego profile mix-ins multilib
        fi
        ;;
    *)
        ego profile mix-ins no-multilib
        ;;
esac
ego profile list

# this sets python targets to build even though we will be mangling USE soon...
# without this, python targets are all unset.
PYTHON_TARGETS="$(portageq envvar PYTHON_TARGETS)"
export PYTHON_TARGETS
PYTHON_SINGLE_TARGET="$(portageq envvar PYTHON_SINGLE_TARGET)"
export PYTHON_SINGLE_TARGET

gcc-config "$(gcc-config -l|grep -c -e "\[")"
gcc -v

# set USE to expanded version of stage1 USE:
EXTRA_USE="$(python /tmp/stage2-bootstrap.py --use)"
# nls multilib nls nptl userland_GNU
export USE="bootstrap $EXTRA_USE"

echo "[ST2PORTAGE] - Upgrading portage"
USE="${USE} reference build" emerge -qj portage || die "[ST2PORTAGE] - Error: failed to emerge portage"

# adding oneshot below so "libtool" doesn't get added to the world file...
# libtool should be in the system profile, but is not currently there it seems.

echo "[ST2SYSTEM] - Bootstrapping minimal system"
emerge $eopts --oneshot -pvt $(python /tmp/stage2-bootstrap.py --pkglist)
emerge $eopts --oneshot $(python /tmp/stage2-bootstrap.py --pkglist) || die "[ST2BOOTSTRAP] - Error: failed to bootstrap minimal system"
# sys-apps/texinfo sys-devel/binutils sys-devel/gcc sys-apps/baselayout sys-libs/zlib virtual/shadow
echo "[ST2SHADOW] - Running grpck"
grpck
echo "[ST2CLEANUP] - Performing some cleanup"
emerge --clean
# removes all but the highest version of a package from your system
emerge --prune --verbose sys-devel/gcc

## LFS Notes:
## Binutils-2.31.1, gcc-8.2.0, Linux-headers, Glibc-2.28,  Libstdc++ from GCC-8.2.0, Binutils-2.31.1 - Pass 2
## GCC-8.2.0 - Pass 2, Tcl-8.6.8, Expect-5.45.4, DejaGNU-1.6.1, M4-1.4.18, Ncurses-6.1, Bash-4.4.18, Bison-3.0.5
## Bzip2-1.0.6, Coreutils-8.30, Diffutils-3.6, File-5.34, Findutils-4.6.0, Gawk-4.2.1, Gettext-0.19.8.1, Grep-3.1
## Gzip-1.9, Make-4.2.1, Patch-2.7.6, Perl-5.28.0, Sed-4.5, Tar-1.30, Texinfo-6.5, Util-linux-2.32.1, Xz-5.2.4

# Currently, a minimal, barely functional Python is installed. Upgrade to
# a full-featured Python installation to avoid problems during the stage3
# build:

ego profile flavor core

for atom in $(portageq match / dev-lang/python|sed -E 's/.*\/([a-zA-Z]+)-([0-9]+\.[0-9]+)\..*/\1:\2/')
# dev-lang/python-2.7.15 dev-lang/python-3.6.6
do
    echo "[ST2PYTHON] Upgrading python to full-featured instalation ..."
    echo "[ST2PYTHON] Old version was $atom"
    # disabling slite since it was creating a circular dependency for python
    # (dev-db/sqlite-3.39.2:3/3::liguros-repo, ebuild scheduled for merge) depends on
    # (dev-libs/icu-71.1-r1:0/71.1::liguros-repo, ebuild scheduled for merge) (buildtime_slot_op)
    #  (dev-lang/python-3.10.6:3.10/3.10::liguros-repo, ebuild scheduled for merge) (buildtime)
    #   (dev-db/sqlite-3.39.2:3/3::liguros-repo, ebuild scheduled for merge) (buildtime_slot_op)
    USE=-sqlite emerge $eopts --oneshot ${atom} || die "[ST2PYTHON] - Error: Failed to upgrade python"
done

eselect python update

# Why running perl-cleaner here? Perl version should be the same as in stage1. Removing for now
#echo "Perl was upgraded let's run perl-cleaner ..."
#perl-cleaner --reallyall -- ${eopts}

# cleanup old perl modules
PERL_MOD_PATH="/usr/lib/perl5/vendor_perl/"
latest_perlmod=$(ls -1r ${PERL_MOD_PATH} |head -1) # eg. 5.30.3
# this find command is going to remove all other vendor_perl dirs of lower versions than the currently latest
find $PERL_MOD_PATH -maxdepth 1 ! -path $PERL_MOD_PATH ! -path "$PERL_MOD_PATH""$latest_perlmod" -type d -exec rm -rf {} \;

gcc-config "$(gcc-config --get-current-profile)"

# remove any remaining cruft in cached files...
env-update

# part of (PYTHON_PROBLEM)
rm /etc/portage/make.conf
# end of (PYTHON_PROBLEM)
