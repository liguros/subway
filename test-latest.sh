#!/bin/bash -xe

echo "Importing the latest stage as image into LXD"

# Check if the image to be imported already exists => delete it if true
# Define the image name you want to check
image_name="liguros-stage3-$LIG_RELEASE-$LIG_VARIANT"

# Use incus image list to list all images in the local store
image_list=$(incus image list)

# Check if the image_name is in the image_list
if [[ $image_list =~ $image_name ]]; then
    echo "Image $image_name exists in the local incus image store."
    incus image delete liguros-stage3-$LIG_RELEASE-$LIG_VARIANT
else
    echo "Image $image_name does not exist in the local incus image store."
fi

incus image list
incus image import images/lxd-$LIG_RELEASE-$LIG_VARIANT.tar.xz --alias liguros-stage3-$LIG_RELEASE-$LIG_VARIANT
incus list

# Check if container still exists => delete it if true
# Define the image name you want to check
container_name="latest-test"

# Use incus image list to list all images in the local store
container_list=$(incus list)

# Check if the image_name is in the image_list
if [[ $icontainer_list =~ $container_name ]]; then
    echo "Container $container_name exists."
    incus stop -f latest-test
    incus delete -f latest-test
else
    echo "Container $container_name does not exist."
fi

incus launch -e liguros-stage3-$LIG_RELEASE-$LIG_VARIANT latest-test
sleep 10
incus exec latest-test -- ps auxf
incus exec latest-test -- ego sync
incus stop latest-test
echo "Latest $LIG_RELEASE - $LIG_VARIANT image successfully started on lxd."
