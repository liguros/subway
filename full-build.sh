#!/bin/bash -xe
set -x
set -eo pipefail

# Logging function
log() {
    local level="$1"
    shift
    echo "[$level] $@"
}

log_info() {
    log "INFO" "$@"
}

log_error() {
    log "ERROR" "$@"
}

log_info "####################################"
log_info "# Welcome to stage building script #"
log_info "####################################"

LIG_RELEASE=$1
LIG_VARIANT=$2
STAGE_STEP=$3

# Check if the required variables are set
if [ -z "$LIG_RELEASE" ] || [ -z "$LIG_VARIANT" ] || [ -z "$STAGE_STEP" ]; then
    log_error "Usage: FULL=true $0 <LIG_RELEASE> <LIG_VARIANT> <STAGE_STEP>"
    log_error "Please provide values for all three variables."
    exit 1
fi

log_info "LIG_RELEASE: $LIG_RELEASE"
log_info "LIG_VARIANT: $LIG_VARIANT"
log_info "STAGE_STEP: $STAGE_STEP"

if [ "$FULL" == "true" ]; then
  log_info "Doing a Full build - Stage1 => Stage2 => Stage3"
else
  log_info "Doing a Stage3 rebuild only."
fi

# Check if stage-functions.sh exists
if [ ! -f "./stage-functions.sh" ]; then
    log_error "Error: stage-functions.sh not found."
    exit 1
fi

log_info "Reading stage-functions.sh"
. ./stage-functions.sh

log_info "Creating artifacts directories"
MIRROR_DIR="${CI_PROJECT_DIR}/images"
install -d "$MIRROR_DIR/logs"

checkOptions
if [ "$FULL" == "true" ]; then
  if [ "$STAGE_STEP" == "stage1" ]; then
    checkBaseImage
    buildStage1
  elif [ "$STAGE_STEP" == "stage2" ]; then
    buildStage2
  elif [ "$STAGE_STEP" == "stage3" ]; then
    buildStage3
    createArchives
    generateLinksSums
  fi
else
    buildStage3
    createArchives
    generateLinksSums
fi

log_info "#####################################################"
log_info "# SUCCESS - All went well new stages are available! #"
log_info "#####################################################"
exit 0
